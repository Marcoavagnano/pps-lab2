package ex1

object Es2 extends App :


  val p1: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z
  val p2 = (x: Int, y: Int, z: Int) => x <= y && y <= z

  def p3(x: Int)(y: Int)(z: Int): Boolean = x match
    case x if x <= y => y <= z
    case _ => false

  def p4(x: Int, y: Int, z: Int): Boolean = x match
    case x if x <= y && y <= z => true
    case _ => false


