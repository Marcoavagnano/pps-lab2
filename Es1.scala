package ex1

object Es1 extends App :
  def parity(x: Int): String = x match
    case x if x % 2 == 0 => "even"
    case _ => "odd"

  val f = (x: Int) => x * x

  def neg(x: String => Boolean): String => Boolean = s => !x(s)

  def neg2[X](x: X => Boolean): X => Boolean = s => !x(s)

  val empty: String => Boolean = _ == ""
  val notEmpty = neg2(empty)
  println(notEmpty("foo") && !notEmpty(""))