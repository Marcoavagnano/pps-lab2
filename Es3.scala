package ex1

object Es3 extends App :
  def compose(f: Int => Int, g: Int => Int): Int => Int =
    x => f(g(x))

  println(compose(_ - 1, _ * 2)(5))

  def composeGen[X](f: X => X, g: X => X): X => X = 
    a => f(g(a))

  println(composeGen[Int](_ - 1, _ * 0)(0))
