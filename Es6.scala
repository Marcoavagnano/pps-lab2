package ex1

object Es6 extends App :

  import Option.*

  enum Option[A]:
    case Some(a: A)
    case None()

  object Option:
    def isEmpty[A](opt: Option[A]): Boolean = opt match
      case None() => true
      case _ => false

    def orElse[A, B >: A](opt: Option[A], orElse: B): B = opt match
      case Some(a) => a
      case _ => orElse

    def flatMap[A, B](opt: Option[A])(f: A => Option[B]): Option[B] = opt match
      case Some(a) => f(a)
      case _ => None()

    def filter[A](opt: Option[A])(f: A => Boolean): Option[A] = opt match
      case Some(a) if f(a) => Some(a)
      case _ => None()

    def map[A, B](opt: Option[A])(f: A => B): Option[B] = opt match
      case Some(a) => Some(f(a))
      case _ => None()

    def map2[A, B >: A](opt1: Option[A], opt2: Option[B])(f: (A, B) => B): Option[B] = (opt1, opt2) match
      case (Some(a), Some(b)) => Some(f(a, b))
      case _ => None()

  println(map2(Some(50), Some(2))((a, b) => a + b))