package ex1
import org .junit.*
import org.junit.Assert.*
import Es5.Shape.*
class ShapeTest:

  @Test
  def testSquarePerimeter () =
    assertEquals(8.0 , perimeter(Square(2)),0)
  @Test
  def testRectanglePerimeter () =
      assertEquals(10.0 , perimeter(Rectangle(2,3)),0)
  @Test
  def testCirclePerimeter () =
      assertEquals(18.8 , perimeter(Circle(3)),0.05)
  @Test
  def testSquareArea () =
    assertEquals(4.0 , area(Square(2)),0)
  @Test
  def testRectangleArea () =
    assertEquals(6.0 , area(Rectangle(2,3)),0)
  @Test
  def testCircleArea () =
    assertEquals(28.3 , area(Circle(3)),0.05)


