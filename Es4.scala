package ex1

object Es4 extends App :
  def fib(n: Int): Int = n match
    case 0 => 0
    case 1 => 1
    case _ => fib(n - 1) + fib(n - 2)

  println(fib(8))

  def tail_fib(n: Int): Int =
    @annotation.tailrec
    def _fib(acc: Int, inc: Int, x: Int): Int = x match
      case 0 => acc
      case _ => _fib(inc, acc + inc, x - 1)

    _fib(0, 1, n)

  println(tail_fib(8))