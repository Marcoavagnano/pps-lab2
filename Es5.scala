package ex1

object Es5 extends App :
  enum Shape:
    case Square(side: Int)
    case Circle(radius: Int)
    case Rectangle(longSide: Int, shortShort: Int)

  object Shape:
    def perimeter(value: Shape): Double = value match
      case Shape.Square(s) => s * 4
      case Shape.Rectangle(sS, sL) => sS * 2 + sL * 2
      case Shape.Circle(r) => 2 * r * 3.14

    def area(value: Shape): Double = value match
      case Shape.Square(s) => s * s
      case Shape.Rectangle(sS, sL) => sS * sL
      case Shape.Circle(r) => r * r * 3.14